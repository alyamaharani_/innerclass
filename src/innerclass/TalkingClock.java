/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclass;



import java.awt.*; 
import java.awt.event.*;  
import java.util.*;  
import javax.swing.*;
import javax.swing.Timer;
/**
 *
 * @author ASUS
 */
class TalkingClock {
    private int interval;
    private boolean beep;
    
    public TalkingClock(int interval, boolean beep)
    /* Constructor class TalkingClock 
            Mengisi field interval dan beep
        - Alya */
    {
        this.interval = interval;
        this.beep = beep;
    }
    
    public void start()
    {
        ActionListener listener = new TimePrinter(); 
        Timer t = new Timer(interval, listener); 
        t.start(); 
    }
    
    public class TimePrinter implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            System.out.println("At the tone, the time is "+new Date());
            if(beep) Toolkit.getDefaultToolkit().beep();
        }
    }
    
}
